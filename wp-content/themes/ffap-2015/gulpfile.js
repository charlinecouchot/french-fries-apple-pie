/**
 *
 * Gulpfile setup
 *
 * @since 1.0.0
 * @authors Charline Couchot
 * @package FFAP
 */


// Configuration du projet
var project = 'ffap-2015',
    url = 'frenchfriesandapplepie.dev',
    bower = './assets/bower_components/';
    build = './ffap-2015/',
    buildInclude = [
        '**/*.php',
        '**/*.html',
        '**/*.css',
        '**/*.js',
        '**/*.svg',
        '**/*.ttf',
        '**/*.otf',
        '**/*.eot',
        '**/*.woff',
        '**/*.woff2',

        'screenshot.png',

        '!node_modules/**/*',
        '!assets/bower_components/**/*',
        '!style.css.map',
        '!assets/js/custom/*',
        '!assets/css/patrials/*'
    ];


var gulp = require('gulp'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-uglifycss'),
    filter = require('gulp-filter'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    newer = require('gulp-newer'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cmq = require('gulp-combine-media-queries'),
    runSequence = require('gulp-run-sequence'),
    sass = require('gulp-sass'),
    plugins = require('gulp-load-plugins')({
        camelize: true
    }),
    ignore = require('gulp-ignore'),
    rimraf = require('gulp-rimraf'),
    zip = require('gulp-zip'),
    plumber = require('gulp-plumber'),
    cache = require('gulp-cache'),
    sourcemaps = require('gulp-sourcemaps');


// BROWSER SYNC
gulp.task('browser-sync', function () {
    var files = [
        '**/*.php',
        '**/*.{png,jpg,gif}'
    ];
    browserSync.init(files, {
        watchTask: true,
        open: 'external',
        host: url,
        proxy: url,
        port: 3000,
        injectChanges: true
    });
});

// SASS PROCESS
gulp.task('styles', function () {
    gulp.src('./assets/css/*.scss')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass({errLogToConsole: true,outputStyle: 'compact',precision: 10}))
        .pipe(sourcemaps.write({includeContent: false}))
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(autoprefixer('last 2 version', '> 1%', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(sourcemaps.write('.'))
        .pipe(plumber.stop())
        .pipe(gulp.dest('./'))
        .pipe(filter('**/*.css'))
        //.pipe(cmq({log: true}))
        .pipe(reload({stream: true}))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss({maxLineLen: 80}))
        .pipe(gulp.dest('./'))
        .pipe(reload({stream: true}))
        .pipe(notify({message: 'Styles task complete',onLast: true}))
});


// VENDOR SCRIPTS
gulp.task('vendorsJs', function () {
    return gulp.src(['./assets/js/vendor/*.js', bower + '**/*.js'])
        .pipe(concat('vendors.js'))
        .pipe(gulp.dest('./assets/js'))
        .pipe(rename({basename: "vendors",suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('./assets/js/'))
        .pipe(notify({message: 'Vendor scripts task complete',onLast: true}));
});


// CUSTOMS SCRIPTS
gulp.task('scriptsJs', function () {
    return gulp.src('./assets/js/custom.js')
        .pipe(rename({basename: "custom",suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('./assets/js/'))
        .pipe(notify({message: 'Custom scripts task complete',onLast: true}));
});


// IMAGES
gulp.task('images', function () {
    // Add the newer pipe to pass through newer images only
    return gulp.src(['./assets/images/**/*.{png,jpg,gif}'])
        .pipe(newer('./assets/images/'))
        .pipe(rimraf({force: true}))
        .pipe(imagemin({optimizationLevel: 7,progressive: true,interlaced: true}))
        .pipe(gulp.dest('./assets/images/'))
        .pipe(notify({message: 'Images task complete',onLast: true}));
});


// FEMME DE MENAGE
gulp.task('clear', function () {
    cache.clearAll();
});


// PREPARATION DU ZIP
gulp.task('cleanup', function () {
    return gulp.src(['./assets/bower_components', '**/.sass-cache', '**/.DS_Store'], {read: false})
        .pipe(ignore('node_modules/**')) // Exemple de fichier à ignorer
        .pipe(rimraf({force: true}))
});
gulp.task('cleanupFinal', function () {
    return gulp.src(['./assets/bower_components', '**/.sass-cache', '**/.DS_Store'], {
            read: false
        }) // much faster
        .pipe(ignore('node_modules/**')) // Exemple de fichier à ignorer
        .pipe(rimraf({
            force: true
        }))
});

// BUILDING
// buildFiles copies all the files in buildInclude to build folder - check variable values at the top buildImages copies all the images from img folder in assets while ignoring images inside raw folder if any
gulp.task('buildFiles', function () {
    return gulp.src(buildInclude)
        .pipe(gulp.dest(build))
        .pipe(notify({message: 'Copy from buildFiles complete',onLast: true}));
});


// OPTIMISATION DES IMAGES
gulp.task('buildImages', function () {
    return gulp.src('assets/images/**/*')
        .pipe(gulp.dest(build + 'assets/images/'))
        .pipe(plugins.notify({message: 'Images copied to buildTheme folder',onLast: true}));
});

// CONSTRUCTION DU ZIP
gulp.task('buildZip', function () {
    return gulp.src(build + '/**/')
        .pipe(zip(project + '.zip'))
        .pipe(gulp.dest('./'))
        .pipe(notify({message: 'Zip task complete',onLast: true}));
});


// NETTOYAGE ET CONSTRUCTION DU ZIP INSTALLABLE
gulp.task('build', function (cb) {
    runSequence('styles', 'cleanup', 'vendorsJs', 'scriptsJs', 'buildFiles', 'buildImages', 'buildZip', 'cleanupFinal', cb);
});


// TÂCHE PAR DEFAUT
gulp.task('default', ['styles', 'vendorsJs', 'scriptsJs', 'images'], function () {
    //gulp.watch('./assets/images/**/*', ['images']);
    //gulp.watch('./assets/css/**/*.scss', ['styles']);
    //gulp.watch('./assets/js/**/*.js', ['scriptsJs', browserSync.reload]);

});