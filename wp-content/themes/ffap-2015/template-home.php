<?php 
/* Template Name: Page d'accueil */
get_header(); ?>
<section class="main-content" id="home-content">
    <?php $recentsNb =  $ffap_options['home-recents-nb'];
          $args = array (
            'posts_per_page' => $recentsNb
    ); ?>
    <?php $query = new WP_Query($args); ?>
    <?php if ($query->have_posts()) : ?>
        <section class="home-section recents">
           <h2 class="home-section--title"><span><?php _e( 'Les derniers articles', 'ffap' ); ?></span></h2>
            <ul>
                <?php while ($query->have_posts()) : $query->the_post(); ?>
                   <li class="home-section--item">
                        <div class="home-section--item_header">
                            <a href="<?php the_permalink() ?>">
                                <span class="home-section--item_icon">
                                    <i class="<?php
                                        $terms = get_the_terms( get_the_ID(), 'category');
                                        if( !empty($terms) ) {
                                            $term = array_pop($terms);
                                            echo get_field('icon-category', $term );
                                        }
                                    ?>"></i>
                                </span>
                                <figure class="home-section--item_img">
                                    <?php if ( has_post_thumbnail() ) { ?> 
                                        <?php the_post_thumbnail('home-article', array('title' => '', 'class' => 'featured--item_img')); ?>
                                    <?php } ?>
                                </figure>
                                <div class="home-section--item_hover">
                                    <span class="home-section--item_date"><?php echo get_the_date('d F Y'); ?></span>
                                    <div class="home-section--item_excerpt">
                                        <?php
                                            $excerpt = get_the_excerpt();
                                            $getlength = strlen($excerpt);
                                            $thelength = 80;
                                            echo mb_substr($excerpt, 0, $thelength);
                                            if ($getlength > $thelength) echo "...";
                                        ?>
                                    </div>
                                </div>
                                <span class="home-section--item_comments"><?php echo comments_number( '0', '1', '%' ); ?> <i class="fa fa-comments"></i></span>
                            </a>
                        </div>
                       <h3 class="home-section--item_title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                    </li>
                <?php endwhile; ?>
            </ul>
            <a class="home-section--more" href="<?php echo get_permalink( get_option( 'page_for_posts' ) );?>"><?php _e( 'Voir tous les articles', 'ffap' ); ?></a>
        </section>
        <?php wp_reset_postdata(); ?>
        <ins id="ads_1" style="display: none;" class="adsbygoogle-placeholder"></ins>
    <?php endif; ?>
    
    <?php $categories = $ffap_options['home-categories'];
          $categoriesNb =  $ffap_options['home-categories-nb'];
          $categoryId = 1 ; ?>
    <?php if($categories) :
        foreach($categories as $categorie) { ?>
           <?php $categoryId++ ?>
           <?php $args = array (
                'posts_per_page' => $categoriesNb,
                'cat' => $categorie
            ); ?>
            <?php $query = new WP_Query($args); ?>
            <?php if ($query->have_posts()) : ?>
                <section class="home-section category">
                   <h2 class="home-section--title"><span><?php echo get_cat_name( $categorie ); ?></span></h2>
                    <ul>
                       <?php while ($query->have_posts()) : $query->the_post(); ?>
                            <li class="home-section--item">
                                <div class="home-section--item_header">
                                    <a href="<?php the_permalink() ?>">
                                        <span class="home-section--item_icon">
                                            <i class="<?php
                                                $terms = get_the_terms( get_the_ID(), 'category');
                                                if( !empty($terms) ) {
                                                    $term = array_pop($terms);
                                                    echo get_field('icon-category', $term );
                                                }
                                            ?>"></i>
                                        </span>
                                        <figure class="home-section--item_img">
                                            <?php if ( has_post_thumbnail() ) { ?> 
                                                <?php the_post_thumbnail('home-article', array('title' => '', 'class' => 'featured--item_img')); ?>
                                            <?php } ?>
                                        </figure>
                                        <div class="home-section--item_hover">
                                            <span class="home-section--item_date"><?php echo get_the_date('d F Y'); ?></span>
                                            <div class="home-section--item_excerpt">
                                                <?php
                                                    $excerpt = get_the_excerpt();
                                                    $getlength = strlen($excerpt);
                                                    $thelength = 80;
                                                    echo mb_substr($excerpt, 0, $thelength);
                                                    if ($getlength > $thelength) echo "...";
                                                ?>
                                            </div>
                                        </div>
                                        <span class="home-section--item_comments"><?php echo comments_number( '0', '1', '%' ); ?> <i class="fa fa-comments"></i></span>
                                    </a>
                                </div>
                               <h3 class="home-section--item_title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                    <a class="home-section--more" href="<?php echo get_category_link($categorie);?>"><?php _e( 'Voir tous les articles dans « '. get_cat_name( $categorie ) .' »', 'ffap' ); ?></a>
                </section>
                <ins id="ads_<?php echo $categoryId; ?>" style="display: none;" class="adsbygoogle-placeholder"></ins>
            <?php endif; ?>
        <?php } ?>
    <?php endif; ?>
</section>  
<aside class="sidebar">
    <?php if ( is_active_sidebar( 'actu-sidebar' ) ) : ?>
        <?php dynamic_sidebar( 'actu-sidebar' ); ?>
    <?php endif; ?>
</aside>
<?php get_footer(); ?>
