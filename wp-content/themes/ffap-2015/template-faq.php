<?php
/*
Template Name: Page de FAQ
*/ get_header(); ?>
<section class="fullwidth-content page--faq" id="page-content">
    <?php the_title( '<h1 class="page--title"><span>', '</span></h1>' ); ?>
    <div class="fullwidth-content--container">
        <div class="page--faq_intro"><?php the_content(); ?></div>
        <div class="page--faq_summary">
            <?php if( have_rows('questions') ): ?>
               <?php $i = 1; ?>
                <ul>

                    <?php while( have_rows('questions') ): the_row(); 
                        $title = get_sub_field('question-title');
                    ?>

                        <li class="question--item">
                            <a href="#<?php echo $i; ?>" class="question--item_title"><?php echo $title; ?></a>
                        </li>
                        <?php $i++; ?>
                    <?php endwhile; ?>

                </ul>
            <?php endif; ?>
        </div>
        <div class="page--faq_questions">
            <?php if( have_rows('questions') ): ?>
               <?php $i = 1; ?>
                <ul>

                    <?php while( have_rows('questions') ): the_row(); 

                        // vars
                        $title = get_sub_field('question-title');
                        $answer = get_sub_field('question-answer');

                        ?>

                        <li id="<?php echo $i; ?>" class="question--item">
                            <h2 class="question--item_title"><?php echo $title; ?></h2>
                            <div class="question--item_answer">
                                <?php echo $answer; ?>
                                <?php $i++; ?>
                            </div>
                        </li>
                    <?php endwhile; ?>

                </ul>
            <?php endif; ?>
        </div>
    </div>
</section>
<?php get_footer(); ?> 