<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search. 
 */
?>

<li class="home-section--item">
    <div class="home-section--item_header">
        <a href="<?php the_permalink() ?>">
            <span class="home-section--item_icon">
                <i class="<?php
                    $terms = get_the_terms( get_the_ID(), 'category');
                    if( !empty($terms) ) {
                        $term = array_pop($terms);
                        echo get_field('icon-category', $term );
                    }
                ?>"></i>
            </span>
            <?php if ( has_post_thumbnail() ) { ?> 
               <figure class="home-section--item_img">
                    <?php the_post_thumbnail('home-article', array('title' => '', 'class' => 'featured--item_img')); ?>
                </figure>
            <?php } ?>
            <div class="home-section--item_hover">
                <p class="home-section--item_date"><?php echo get_the_date('d F Y'); ?></p>
                <p class="home-section--item_excerpt">
                    <?php
                        $excerpt = get_the_excerpt();
                        $getlength = strlen($excerpt);
                        $thelength = 80;
                        echo mb_substr($excerpt, 0, $thelength);
                        if ($getlength > $thelength) echo "...";
                    ?>
                </p>
            </div>
            <span class="home-section--item_comments"><?php echo comments_number( '0', '1', '%' ); ?> <i class="fa fa-comments"></i></span>
        </a>
    </div>
   <h3 class="home-section--item_title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
</li>