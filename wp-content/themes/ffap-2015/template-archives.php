<?php
/*
Template Name: Archives
*/ get_header(); ?>
<section class="fullwidth-content" id="page-content">
   <?php the_title( '<h1 class="archive--title"><span>', '</span></h1>' ); ?>
   <?php the_content(); ?>
   <?php if (function_exists('simpleYearlyArchive')) {simpleYearlyArchive();} ?>
</section>
<?php get_footer(); ?> 