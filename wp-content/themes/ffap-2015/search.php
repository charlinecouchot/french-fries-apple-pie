<?php get_header(); ?>
<section class="main-content"  id="home-content">
    <?php if (have_posts()) : ?>
        <section class="home-section">
           <h2 class="home-section--title">
              <span>
                  <?php echo _e( 'Résultats de recherche pour : "', 'sbs' ).get_search_query().'"'; ?>
              </span>
            </h2>
            <ul id="article-list">
                <?php
                    while ( have_posts() ) : the_post();
                        get_template_part( 'content', get_post_format() );
                    endwhile;
                ?>
            </ul>
            <button class="btn btn-turquoise" id="infinite-handle">  
                <?php _e( 'Charger plus d\'articles', 'ffap' ); ?>
            </button>  
        </section> 
        <?php wp_reset_postdata(); ?>
    <?php endif; ?>
</section>  
<aside class="sidebar">
    <?php if ( is_active_sidebar( 'actu-sidebar' ) ) : ?>
        <?php dynamic_sidebar( 'actu-sidebar' ); ?>
    <?php endif; ?>
</aside>
<?php get_footer(); ?>
