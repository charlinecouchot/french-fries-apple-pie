<?php

// Modèle de commentaires -----------------------------------------
function commentsTemplate($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment;
   extract($args, EXTR_SKIP);
    if ( 'div' == $args['style'] ) {
        $tag = 'div';
        $add_below = 'comment';
    } else {
        $tag = 'li';
        $add_below = 'div-comment';
    }
?>


<<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
    <?php if ( 'div' != $args['style'] ) : ?>
        <div class="comment--container">
    <?php endif; ?>
    <?php /*get_option( 'admin_email' );*/ if (get_comment_author_email() == 'contact@frenchfriesandapplepie.com') { ?><div class="comment--author-ribbonwrapper"><div class="comment--author-ribbon"><?php _e('Auteur', 'ffap'); ?></div></div><?php } ?>
        <header class="comment--header">
            <?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
            <div class="comment--meta">
                <span class="comment--author"><?php echo get_comment_author_link(); ?></span>
                <span class="comment--datetime"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>"><?php printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time('H\hi') ); ?></a></span>
            </div>
        </header> 
        <div class="comment--body">
           <?php if ( $comment->comment_approved == '0' ) : ?><span class="comment--moderation"><?php _e( 'Your comment is awaiting moderation.' ); ?></span><?php endif; ?>
           <?php comment_text(); ?> 
        </div>
        <footer class="comment--footer">
          <span class="comment--edit"><?php edit_comment_link( __( 'Edit' ), '  ', '' ); ?></span>
          <span class="comment--reply"><?php comment_reply_link( array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?></span>
        </footer>
    <?php if ( 'div' != $args['style'] ) : ?>
        </div>
    <?php endif; ?>
<?php }
// ----------------------------------------------------------------

// Changer le titre du modèle de commentaire ----------------------
function my_comment_form_before() {
    ob_start();
}
add_action( 'comment_form_before', 'my_comment_form_before' );

function my_comment_form_after() {
    $html = ob_get_clean();
    $html = preg_replace(
        '/<h3 id="reply-title" class="comment-reply-title">(.*)<\/h3>/',
        '<h1 id="reply-title" class="comments--reply-title">\1</h1>',
        $html
    );
    echo $html;
}
add_action( 'comment_form_after', 'my_comment_form_after' );
// ----------------------------------------------------------------


?>