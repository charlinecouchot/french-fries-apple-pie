<?php
/*
-----------------------------------------------------------------------------------

 	Plugin Name: Social Counter Sidebar Widget
 	Version: 1.0
 	Author: Zerge
 	Author URI:  http://www.color-theme.com
 
-----------------------------------------------------------------------------------
*/


/**
 * Add function to widgets_init that'll load our widget.
 */

add_action( 'widgets_init', create_function( '', 'register_widget("ffap_welcome_widget");' ) );


class ffap_welcome_widget extends WP_widget{

	public function __construct() {
		$widget_ops = array( 'classname' => 'welcome', 'description' => __( 'Widget de bienvenue spécialement créé pour FFAP.' , 'ffap-2015' ) );        
        parent::__construct( 'ffap_welcome_widget', __( 'Bienvenue' , 'ffap-2015' ), $widget_ops );

        add_action('admin_enqueue_scripts', array($this, 'upload_scripts'));
	}
    
    public function upload_scripts() {
        wp_enqueue_media();
        wp_enqueue_script('media-upload');
        wp_enqueue_script('upload_media_widget', get_template_directory_uri() .'/assets/admin/widgets/welcome/js/media-upload.js');
    }
	
	function widget($args,$instance) {
		extract($args);
		
			$title           = $instance['title'];
			$welcome_desc    = $instance['welcome_desc'];
			$welcome_more    = $instance['welcome_more'];
			$facebook        = $instance['facebook'];
			$twitter         = $instance['twitter'];
			$hellocoton      = $instance['hellocoton'];
			$googleplus      = $instance['googleplus'];
			$pinterest       = $instance['pinterest'];
			$instagram       = $instance['instagram'];
			$youtube         = $instance['youtube'];
			$contact         = $instance['contact'];
			
			$show_facebook   = isset($instance['show_facebook']) ? 'true' : 'false';
			$show_twitter    = isset($instance['show_twitter']) ? 'true' : 'false';
			$show_hellocoton = isset($instance['show_hellocoton']) ? 'true' : 'false';
			$show_googleplus = isset($instance['show_googleplus']) ? 'true' : 'false';									
			$show_pinterest  = isset($instance['show_pinterest']) ? 'true' : 'false';
			$show_instagram  = isset($instance['show_instagram']) ? 'true' : 'false';
			$show_youtube    = isset($instance['show_youtube']) ? 'true' : 'false';
			$show_rss        = isset($instance['show_rss']) ? 'true' : 'false';
			$show_contact    = isset($instance['show_contact']) ? 'true' : 'false';									
        ?>


		<?php echo $before_widget; ?>
            <?php if ( $title ){ echo $before_title . $title . $after_title; } ?>
            <?php $admin_email = get_option( 'admin_email' ); ?>
            <div class="welcome--intro">
                <p>
                    <span class="welcome--img"><?php echo get_avatar('contact@frenchfriesandapplepie.com', 125 ); ?></span>
                    <?php echo $welcome_desc; ?>
                    <a href="<?php echo get_permalink($welcome_more); ?>" class="welcome--more"><?php _e( 'En savoir plus' , 'ffap-2015'); ?></a>

                </p>
            </div>
            <ul class="welcome--social"> 
                <?php if ( $show_facebook == 'true') : ?>
                    <li class="welcome--social_item facebook"><a href="https://www.facebook.com/<?php echo $facebook; ?>" target="_blank"><i class="icon-facebook" aria-hidden="true"></i> Facebook</a></li>
                <?php endif; ?>
                <?php if ( $show_twitter == 'true') : ?> 
                    <li class="welcome--social_item twitter"><a href="https://www.twitter.com/<?php echo $twitter; ?>" target="_blank"><i class="icon-twitter" aria-hidden="true"></i>Twitter</a></li>
                <?php endif; ?>
                <?php if ( $show_hellocoton == 'true') : ?>
                    <li class="welcome--social_item hellocoton"><a href="http://www.hellocoton.com/mapage/<?php echo $hellocoton; ?>" target="_blank"><i class="icon-hellocoton" aria-hidden="true"></i>Hellocoton</a></li>
                <?php endif; ?>
                <?php if ( $show_googleplus == 'true') : ?>
                    <li class="welcome--social_item google-plus"><a href="https://plus.google.com/u/0/+<?php echo $googleplus; ?>/posts" target="_blank"><i class="icon-google-plus" aria-hidden="true"></i>Google Plus</a></li>
                <?php endif; ?>
                <?php if ( $show_pinterest == 'true') : ?>
                    <li class="welcome--social_item pinterest"><a href="https://www.pinterest.com/<?php echo $pinterest; ?>" target="_blank"><i class="icon-pinterest" aria-hidden="true"></i>Pinterest</a></li>
                <?php endif; ?>
                <?php if ( $show_instagram == 'true') : ?>
                    <li class="welcome--social_item instagram"><a href="https://www.instagram.com/<?php echo $instagram; ?>" target="_blank"><i class="icon-instagram" aria-hidden="true"></i>Instagram</a></li>
                <?php endif; ?>
                <?php if ( $show_youtube == 'true') : ?>
                    <li class="welcome--social_item youtube"><a href="https://www.youtube.com/channel/<?php echo $youtube; ?>" target="_blank"><i class="icon-youtube" aria-hidden="true"></i>Youtube</a></li>
                <?php endif; ?>
                <?php if ( $show_rss == 'true') : ?>
                    <li class="welcome--social_item rss"><a href="<?php echo bloginfo('rss2_url'); ?>" target="_blank"><i class="icon-rss" aria-hidden="true"></i>RSS</a></li>
                <?php endif; ?>
                <?php if ( $show_contact == 'true') : ?>
                    <li class="welcome--social_item contact"><a href="<?php echo get_permalink($contact); ?>" target="_blank"><i class="icon-contact" aria-hidden="true"></i>Contact</a></li>
                <?php endif; ?>
            </ul>
		<?php echo $after_widget;

    } 
    
    function update($new_instance, $old_instance){
		$instance = $old_instance;
        
        $instance['title']              = $new_instance['title'];
        $instance['welcome_desc']       = $new_instance['welcome_desc'];
        $instance['welcome_more']       = $new_instance['welcome_more'];
        $instance['facebook']           = $new_instance['facebook'];
        $instance['twitter']            = $new_instance['twitter'];
        $instance['hellocoton']         = $new_instance['hellocoton'];
        $instance['googleplus']         = $new_instance['googleplus'];
        $instance['pinterest']          = $new_instance['pinterest'];
        $instance['instagram']          = $new_instance['instagram'];
        $instance['youtube']            = $new_instance['youtube'];
        $instance['contact']            = $new_instance['contact'];
        $instance['show_facebook']      = $new_instance['show_facebook'];
        $instance['show_twitter']       = $new_instance['show_twitter'];
        $instance['show_hellocoton']    = $new_instance['show_hellocoton'];
        $instance['show_googleplus']    = $new_instance['show_googleplus'];
        $instance['show_pinterest']     = $new_instance['show_pinterest'];
        $instance['show_instagram']     = $new_instance['show_instagram'];
        $instance['show_youtube']       = $new_instance['show_youtube'];
        $instance['show_rss']           = $new_instance['show_rss'];
        $instance['show_contact']       = $new_instance['show_contact'];  

		return $instance;
	}

	function form($instance){
        $defaults = array( 
        'title'                 => __( 'Bienvenue', 'ffap-2015' ), 
        'welcome_desc'          => '',
        'welcome_more'          => '',
        'facebook'              => '258968560929838',
        'twitter'               => 'Fries_and_Pie',
        'hellocoton'            => 'frenchfriesandapplepie',
        'googleplus'            => 'Frenchfriesandapplepie',
        'pinterest'             => 'FriesandPie',
        'instagram'             => 'fries_and_pie',
        'youtube'               => 'UCd4UGA7sbE8IKtJhNs-SLhA',
        'contact'               => '',
        'show_facebook'         => 'on',
        'show_twitter'          => 'on',
        'show_hellocoton'       => 'on',
        'show_googleplus'       => 'on',
        'show_pinterest'        => 'on',
        'show_instagram'        => 'on',
        'show_youtube'          => 'on',
        'show_rss'              => 'on',
        'show_contact'          => 'on');
        
        $instance = wp_parse_args((array) $instance, $defaults);  ?>

		
        <table>
            <tr>
                <td><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e( 'Titre :' , 'ffap-2015' ); ?></label></td>
                <td>
                    <input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" />
                </td>
            </tr>
            <tr>
                <td vertical-align="top"><label for="<?php echo $this->get_field_id('welcome_desc'); ?>"><?php _e( 'Description :' , 'ffap-2015' ); ?></label></td>
                <td>
                    <textarea rows="6" cols="30" id="<?php echo $this->get_field_id('welcome_desc'); ?>" name="<?php echo $this->get_field_name('welcome_desc'); ?>"><?php echo $instance['welcome_desc']; ?></textarea>
                </td>
            </tr>
           <tr>
                <td><label for="<?php echo $this->get_field_id('welcome_more'); ?>"><?php _e( 'Page "En savoir plus" :' , 'ffap-2015' ); ?></label></td>
                <td>
                   <?php wp_dropdown_pages(
                    array(
                        'id' => $this->get_field_id('welcome_more'),
                        'name' => $this->get_field_name('welcome_more'),
                        'selected' => $instance['welcome_more']
                    ));?>
                </td>
            </tr>
            <tr>
                <td><label for="<?php echo $this->get_field_id('facebook'); ?>"><?php _e( 'Identifiant Facebook :' , 'ffap-2015' ); ?></label></td>
                <td>
                    <input class="checkbox" type="checkbox" <?php checked($instance['show_facebook'], 'on'); ?> id="<?php echo $this->get_field_id('show_facebook'); ?>" name="<?php echo $this->get_field_name('show_facebook'); ?>" />
                    <input id="<?php echo $this->get_field_id('facebook'); ?>" name="<?php echo $this->get_field_name('facebook'); ?>" value="<?php echo $instance['facebook']; ?>" />
                 </td>
            </tr>
            <tr>
                <td><label for="<?php echo $this->get_field_id('twitter'); ?>"><?php _e( 'Identifiant Twitter :' , 'ffap-2015' ); ?></label></td>
                <td>
                    <input class="checkbox" type="checkbox" <?php checked($instance['show_twitter'], 'on'); ?> id="<?php echo $this->get_field_id('show_twitter'); ?>" name="<?php echo $this->get_field_name('show_twitter'); ?>" />
                    <input id="<?php echo $this->get_field_id('twitter'); ?>" name="<?php echo $this->get_field_name('twitter'); ?>" value="<?php echo $instance['twitter']; ?>" />
                </td>
            </tr>
            <tr>
                <td><label for="<?php echo $this->get_field_id('hellocoton'); ?>"><?php _e( 'Identifiant Hello Coton :' , 'ffap-2015' ); ?></label></td>
                <td>
                    <input class="checkbox" type="checkbox" <?php checked($instance['show_hellocoton'], 'on'); ?> id="<?php echo $this->get_field_id('show_hellocoton'); ?>" name="<?php echo $this->get_field_name('show_hellocoton'); ?>" />
                    <input id="<?php echo $this->get_field_id('hellocoton'); ?>" name="<?php echo $this->get_field_name('hellocoton'); ?>" value="<?php echo $instance['hellocoton']; ?>" />
                </td>
            </tr>
            <tr>
                <td><label for="<?php echo $this->get_field_id('googleplus'); ?>"><?php _e( 'Identifiant Google+ :' , 'ffap-2015' ); ?></label></td>
                <td>
                    <input class="checkbox" type="checkbox" <?php checked($instance['show_googleplus'], 'on'); ?> id="<?php echo $this->get_field_id('show_googleplus'); ?>" name="<?php echo $this->get_field_name('show_googleplus'); ?>" />
                    <input id="<?php echo $this->get_field_id('googleplus'); ?>" name="<?php echo $this->get_field_name('googleplus'); ?>" value="<?php echo $instance['googleplus']; ?>" />
                </td>
            </tr>
            <tr>
                <td><label for="<?php echo $this->get_field_id('pinterest'); ?>"><?php _e( 'Identifiant Pinterest :' , 'ffap-2015' ); ?></label></td>
                <td>
                    <input class="checkbox" type="checkbox" <?php checked($instance['show_pinterest'], 'on'); ?> id="<?php echo $this->get_field_id('show_pinterest'); ?>" name="<?php echo $this->get_field_name('show_pinterest'); ?>" />
                    <input id="<?php echo $this->get_field_id('pinterest'); ?>" name="<?php echo $this->get_field_name('pinterest'); ?>" value="<?php echo $instance['pinterest']; ?>" />
                </td>
            </tr>
            <tr>
                <td><label for="<?php echo $this->get_field_id('instagram'); ?>"><?php _e( 'Identifiant Instagram :' , 'ffap-2015' ); ?></label></td>
                <td>
                    <input class="checkbox" type="checkbox" <?php checked($instance['show_instagram'], 'on'); ?> id="<?php echo $this->get_field_id('show_instagram'); ?>" name="<?php echo $this->get_field_name('show_instagram'); ?>" />
                    <input id="<?php echo $this->get_field_id('instagram'); ?>" name="<?php echo $this->get_field_name('instagram'); ?>" value="<?php echo $instance['instagram']; ?>" />
                </td>
            </tr>
            <tr>
                <td><label for="<?php echo $this->get_field_id('youtube'); ?>"><?php _e( 'Identifiant Youtube :' , 'ffap-2015' ); ?></label></td>
                <td>
                    <input class="checkbox" type="checkbox" <?php checked($instance['show_youtube'], 'on'); ?> id="<?php echo $this->get_field_id('show_youtube'); ?>" name="<?php echo $this->get_field_name('show_youtube'); ?>" />
                    <input id="<?php echo $this->get_field_id('youtube'); ?>" name="<?php echo $this->get_field_name('youtube'); ?>" value="<?php echo $instance['youtube']; ?>" />
                </td>
            </tr>
            <tr>
                <td><label for="<?php echo $this->get_field_id('rss'); ?>"><?php _e( 'Afficher RSS' , 'ffap-2015' ); ?></label><br /></td>
                <td><input class="checkbox" type="checkbox" <?php checked($instance['show_rss'], 'on'); ?> id="<?php echo $this->get_field_id('show_rss'); ?>" name="<?php echo $this->get_field_name('show_rss'); ?>" /></td>
            </tr>
            <tr>
                <td><label for="<?php echo $this->get_field_id('contact'); ?>"><?php _e( 'Page de contact :' , 'ffap-2015' ); ?></label></td>
                <td>
                   <input class="checkbox" type="checkbox" <?php checked($instance['show_contact'], 'on'); ?> id="<?php echo $this->get_field_id('show_contact'); ?>" name="<?php echo $this->get_field_name('show_contact'); ?>" />
                    <?php wp_dropdown_pages(
                        array(
                            'id' => $this->get_field_id('contact'),
                            'name' => $this->get_field_name('contact'),
                            'selected' => $instance['contact']
                        ));?>
                </td>
            </tr>
		</table>
		
		
		<?php

	}
}
?>