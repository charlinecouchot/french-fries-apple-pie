<?php

// Enregistrement des widgets -------------------------------------
    function ffap_widgets_init() {
        register_sidebar( array(
            'name'          => __( 'Barre latérale', 'ffap' ),
            'id'            => 'actu-sidebar',
            'description'   => __( 'Ajoutez ici les widgets apparaissant dans la barre latérale de la page d\'accueil et d\'archives.', 'ffap' ),
            'before_title'  => '<h3 class="sidebar--widget_title"><span>',
            'after_title'   => '</span></h3>',
            'before_widget' => '<div class="sidebar--widget %2$s">', 
            'after_widget'  => '</div>',
        ) );
        register_sidebar( array(
            'name' => __( 'Ancienne barre latérale', 'twentytwelve' ),
            'id' => 'sidebar-1',
            'description' => __( 'Appears on posts and pages except the optional Front Page template, which has its own widgets', 'twentytwelve' ),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget' => '</aside>',
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        ) );
    }
    add_action( 'widgets_init', 'ffap_widgets_init' ); 
    require_once get_template_directory() . '/assets/admin/widgets/welcome/welcome.php';
// ----------------------------------------------------------------

?>