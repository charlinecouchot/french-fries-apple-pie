<?php 
// Ajouter les boutons WISIWYG  -----------------------------------
if ( !function_exists('ffap_init_editor_styles')) {
   add_action( 'after_setup_theme', 'ffap_init_editor_styles' );
   function ffap_init_editor_styles() {
	add_editor_style();
   }
}

// Add Formats Dropdown Menu To MCE
if ( ! function_exists( 'wpex_style_select' ) ) {
	function wpex_style_select( $buttons ) {
		array_push( $buttons, 'styleselect' );
		return $buttons;
	}
}
add_filter( 'mce_buttons', 'wpex_style_select' );

function my_mce_before_init_insert_formats( $init_array ) {  
	$style_formats = array(
        array(
            'title'   => __( 'Titres', 'ffap'),
            'items' => array(
                array(
                    'title'   => __( 'Titre 1', 'ffap'),
                    'format'  => 'h2',
                    'icon'    => 'h2-before',
                ),
                array(
                    'title'   => __( 'Titre 2', 'ffap'),
                    'format'  => 'h3',
                    'icon'    => 'h3-before',
                ),
                array(
                    'title'   => __( 'Titre 3', 'ffap'),
                    'format'  => 'h4',
                    'icon'    => 'h4-before',
                ),
                array(
                    'title'   => __( 'Titre 4', 'ffap'),
                    'format'  => 'h5',
                    'icon'    => 'h5-before',
                ),
            ),
        ),
		array(
            'title'	=> __( 'Séparateurs', 'ffap'),
            'items'	=> array(
                array(
                    'title' => __('Arabesque + ligne bleue', 'ffap'),
                    'block' => 'hr',
                ),

                array(
                    'title' => __('Pointillés ocres', 'ffap'),
                    'block' => 'hr',
                    'classes' => 'dotted'
                ),
                array(
                    'title' => __('Arabesque simple', 'ffap'),
                    'block' => 'hr',
                    'classes' => 'arabesque'
                ),
                array(
                    'title' => __('Espace blanc', 'ffap'),
                    'block' => 'hr',
                    'classes' => 'blank'
                ),
            ),
        ),
        array(
            'title'	=> __( 'Couleurs', 'ffap'),
            'items'	=> array(
                array(
                    'title' => __('Ocre', 'ffap'),
                    'inline' => 'span',
                    'classes' => 'txt-ocre',
                ),

                array(
                    'title' => __('Turquoise', 'ffap'),
                    'inline' => 'span',
                    'classes' => 'txt-turquoise',
                ),
            ),
        ),
        array(
            'title' => __('Liste à puce étoile', 'ffap'),
            'selector' => 'ul',
            'classes' => 'ul-star',
        ),
	);  
	
	$init_array['style_formats'] = json_encode( $style_formats );  
	return $init_array;  
  
} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );  

// Hooks your functions into the correct filters
function my_add_mce_button() {
	// check user permissions
	if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
		return;
	}
	// check if WYSIWYG is enabled
	if ( 'true' == get_user_option( 'rich_editing' ) ) {
		add_filter( 'mce_external_plugins', 'my_add_tinymce_plugin' );
		add_filter( 'mce_buttons', 'my_register_mce_button' );
	}
}
add_action('admin_head', 'my_add_mce_button');

// Declare script for new button
function my_add_tinymce_plugin( $plugin_array ) {
	$plugin_array['my_mce_button'] = get_template_directory_uri() .'/assets/js/editor/mce-button.js';
	return $plugin_array;
}

// Register new button in the editor
function my_register_mce_button( $buttons ) {
	array_push( $buttons, 'separator', 'my_mce_button', 'my_mce_button2' );
	return $buttons;
}
// ----------------------------------------------------------------
?>