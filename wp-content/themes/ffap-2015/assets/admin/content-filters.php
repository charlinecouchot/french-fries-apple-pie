<?php 
// Remplacement dans le corps du texte ----------------------------
function filter_ptags_on_images($content)
{
    $content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
    return preg_replace('/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '\1', $content);
}
add_filter('the_content', 'filter_ptags_on_images');

function filter_nbsp($content){
   return preg_replace('/<p>\s*(&nbsp;)\s*<\/p>/iU', '', $content);
}
add_filter('the_content', 'filter_nbsp');

function ffap_img_caption_shortcode($val, $attr, $content = null) {

  extract(shortcode_atts(array(
    'id'    => '',
    'align' => 'alignnone',
    'width' => '',
    'caption' => ''
  ), $attr));

  if ( 1 > (int) $width || empty($caption) ) return $content;

  // Grab ID to query post_content [description] field for image
  preg_match('/([\d]+)/', $id, $matches);

  // get the description html
  $description_html = '';
  if ( $matches[0] ) {
    global $wpdb;
    $custom_description = $wpdb->get_row("SELECT post_content FROM $wpdb->posts WHERE ID = {$matches[0]};");
    if ($custom_description->post_content) {
      $description_html = "<p class=\"caption--description\">{$custom_description->post_content}</p>";
    }
  }

  // get the caption html and other fragments for the attachement
  $caption_html = "<p class=\"caption--title\">{$caption}</p>";
  $id_html      = esc_attr($id);
  $align_html 	= esc_attr($align);
  $style_html 	= 'width:'.$width.'px';
  $content_html = do_shortcode($content);

  return "<figure id=\"{$id_html}\" class=\"caption single--caption {$align_html}\" style=\"{$style_html}\">{$content_html}<figcaption>{$caption_html}{$description_html}</figcaption></figure>";
}
add_filter('img_caption_shortcode', 'ffap_img_caption_shortcode', 10, 3);

function give_linked_images_class($content) {

  $classes = 'single--media'; // separate classes by spaces - 'img image-link'

  // check if there are already a class property assigned to the anchor
  if ( preg_match('/<a.*? class=".*?"><img/', $content) ) {
    // If there is, simply add the class
    $content = preg_replace('/(<a.*? class=".*?)(".*?)(><img)/', '$1 ' . $classes . '$2 rel="group" $3', $content);
  } else {
    // If there is not an existing class, create a class property
    $content = preg_replace('/(<a.*?)><img/', '$1 class="' . $classes . '" rel="group"><img', $content);
  }
  return $content;
}
add_filter('the_content','give_linked_images_class');

function comment_links_in_new_tab($text) {
$return = str_replace('<a', '<a target="_blank"', $text);
return $return;
}
add_filter('get_comment_author_link', 'comment_links_in_new_tab');
add_filter('comment_text', 'comment_links_in_new_tab');

// Articles connexes ----------------------------------------------
function jetpackme_more_related_posts( $options ) {
    $options['size'] = 4;
    return $options;
}
add_filter( 'jetpack_relatedposts_filter_options', 'jetpackme_more_related_posts' );

/*function jetpackme_remove_rp() {
    $jprp = Jetpack_RelatedPosts::init();
    $callback = array( $jprp, 'filter_add_target_to_dom' );
    remove_filter( 'the_content', $callback, 40 );
}
add_filter( 'wp', 'jetpackme_remove_rp', 20 );*/

function jetpackme_related_posts_headline( $headline ) {
$headline = sprintf(
            '<h1 class="jp-relatedposts-title">%s</h1>',
            __('Si vous avez aimé cet article, vous aimerez aussi ceux-là', 'ffap')
            );
return $headline;
}
add_filter( 'jetpack_relatedposts_filter_headline', 'jetpackme_related_posts_headline' );

function jeherve_remove_all_jp_css() {  
  wp_deregister_style( 'jetpack_related-posts' ); //Related Posts 
}
add_filter( 'jetpack_implode_frontend_css', '__return_false' );
add_action('wp_print_styles', 'jeherve_remove_all_jp_css' );

add_filter( 'jetpack_relatedposts_filter_post_context', '__return_empty_string' );
// ----------------------------------------------------------------

// Envoyer à un ami -----------------------------------------------
function email_to_friend($atts) {
    global $post;
    extract(shortcode_atts(array(
        'anchor_text' => 'Envoyer à un ami',
    ), $atts));
 
    $post_title = htmlspecialchars( $post->post_title );
    $email_subject = 'Sur '.htmlspecialchars(get_bloginfo('name')).' : « '.$post_title.' »';
    $email_body = 'Je te recommande cet article : '.$post_title.'. Tu pourras le lire ici : '.get_permalink($post->ID);
    $send_button = '<a class="send-button" rel="nofollow" href="mailto:?subject='.rawurlencode(htmlspecialchars_decode($email_subject)).'&amp;body='.rawurlencode(htmlspecialchars_decode($email_body)).'" title="'.$anchor_text.' : '.$post_title.'"><i class="icon-contact" aria-hidden="true"></i></a>';
    
    return $send_button;
}
add_shortcode('send_to_friend', 'email_to_friend');
// ----------------------------------------------------------------

// Scroll infini -----------------------------------------------
function ffap_infinite_scroll_init() {
    add_theme_support( 'infinite-scroll', array(
        'container' => 'article-list',
        'wrapper'         => false,
        'footer'    => 'footer',
        'type'           => 'click',
        'posts_per_page' => '6',
        'click_handle'   => false
    ) );
} 
add_action( 'after_setup_theme', 'ffap_infinite_scroll_init' );
// ---------------------------------------------------------------- 



add_filter( 'the_content', 'mh_add_prettyphoto' , 15 );
function mh_add_prettyphoto( $text ) {

        // RegEx to find the right <a href=...><img data-attachment-id=... and put that into an array
        $mh_regex = "/<a href=\"(http|https):\/\/[a-zA-Z0-9-.\/\?=]+\"><img\ data-attachment-id=/";

        // Use that RegEx and populate the hits into an array
        preg_match_all( $mh_regex , $text , $mh_matches );

        // If there's any hits then loop though those and replace those hits
        for ( $mh_count = 0; $mh_count < count( $mh_matches[0] ); $mh_count++ )
                {
                        $mh_old = $mh_matches[0][$mh_count];
                        $mh_new = str_replace( '><img data' , ' rel="group"><img data' , $mh_old );
                        $text = str_replace( $mh_old  , $mh_new , $text );
                }
        // Return any substitutions
        return $text;
}
?>
