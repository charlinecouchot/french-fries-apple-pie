<?php 
// Scripts et styles ---------------------------------------------- 
    function ffap_scripts() {
        if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {
            wp_enqueue_script('jquery'); 
            wp_register_script('fancybox', get_template_directory_uri() . '/assets/js/vendor/jquery.fancybox.js', '', true);
            wp_register_script('sticky', get_template_directory_uri() . '/assets/js/vendor/jquery.sticky.js', '', true);
            wp_register_script('raphael', get_template_directory_uri() . '/assets/js/vendor/raphael.js', '', true);
            wp_register_script('mapael', get_template_directory_uri() . '/assets/js/vendor/jquery.mapael.js', '', true);
            wp_register_script('map', get_template_directory_uri() . '/assets/js/vendor/map-states.js', '', true);
            wp_register_script('imagesLoaded', get_template_directory_uri() . '/assets/js/vendor/imagesloaded.min.js', '', true);
            wp_register_script('isotope', get_template_directory_uri() . '/assets/js/vendor/jquery.isotope.min.js', '', true);
            wp_register_script('packery', get_template_directory_uri() . '/assets/js/vendor/jquery.packery.min.js', '', true);
            wp_register_script('customJs', get_template_directory_uri() . '/assets/js/custom.js');
            
            wp_enqueue_script('jquery');
            wp_enqueue_script('raphael');
            wp_enqueue_script('fancybox');
            wp_enqueue_script('sticky');
            wp_enqueue_script('mapael');
            wp_enqueue_script('map');
            wp_enqueue_script('imagesLoaded');
            wp_enqueue_script('isotope');
            wp_enqueue_script('packery');
            wp_enqueue_script('customJs');
        }
    }
    add_action('wp_enqueue_scripts', 'ffap_scripts');

    function ffap_styles() {
        wp_enqueue_style( 'fontawesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' );
        wp_enqueue_style( 'style', get_stylesheet_uri() );
    }
    add_action('wp_enqueue_scripts', 'ffap_styles');

    add_action( 'wp_enqueue_scripts', 'enqueue_comments_reply' );
    function enqueue_comments_reply() {
        if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
            wp_enqueue_script( 'comment-reply' );
        }
    }

    add_editor_style();

    function custom_admin_css() {
        wp_enqueue_style('admin_styles', get_template_directory_uri().'/admin-style.css');
    }

    add_action('admin_head', 'custom_admin_css');

    remove_action( 'wp_footer', 'wp_admin_bar_render', 1000 );
    add_action( 'wp_head', 'wp_admin_bar_render', 1000 );
    add_action( 'wp_head', 'wpse_42041_admin_bar_fix', 1000 );

    function wpse_42041_admin_bar_fix() { ?>
    <style>
        html{margin-top:0 !important}
        #wpadminbar{position:relative !important}
    </style>
    <?php }

// ----------------------------------------------------------------
?>