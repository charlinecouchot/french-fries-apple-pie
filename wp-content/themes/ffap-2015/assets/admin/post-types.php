<?php
// Custom post type liens -----------------------------------------
function links_post_type() {

	$labels = array(
		'name'                  => _x( 'Liens', 'Post Type General Name', 'ffap-2015' ),
		'singular_name'         => _x( 'Lien', 'Post Type Singular Name', 'ffap-2015' ),
		'menu_name'             => __( 'Liens', 'ffap-2015' ),
		'name_admin_bar'        => __( 'Liens', 'ffap-2015' ),
		'archives'              => __( 'Liens archivés', 'ffap-2015' ),
		'parent_item_colon'     => __( 'Lien parent:', 'ffap-2015' ),
		'all_items'             => __( 'Tous les liens', 'ffap-2015' ),
		'add_new_item'          => __( 'Ajouter un lien', 'ffap-2015' ),
		'add_new'               => __( 'Ajouter un lien', 'ffap-2015' ),
		'new_item'              => __( 'Nouveau lien', 'ffap-2015' ),
		'edit_item'             => __( 'Editer un lien', 'ffap-2015' ),
		'update_item'           => __( 'Mettre à jour le lien', 'ffap-2015' ),
		'view_item'             => __( 'Voir le lien', 'ffap-2015' ),
		'search_items'          => __( 'Rechercher un lien', 'ffap-2015' ),
		'not_found'             => __( 'Introuvable', 'ffap-2015' ),
		'not_found_in_trash'    => __( 'Introuvable dans la corbeille', 'ffap-2015' ),
		'featured_image'        => __( 'Image à la une', 'ffap-2015' ),
		'set_featured_image'    => __( 'Sélectionner une image à la une', 'ffap-2015' ),
		'remove_featured_image' => __( 'Retirer une image à la une', 'ffap-2015' ),
		'use_featured_image'    => __( 'Utiliser comme image à la une', 'ffap-2015' ),
		'insert_into_item'      => __( 'Insérer dans le lien', 'ffap-2015' ),
		'uploaded_to_this_item' => __( 'Ajouté à ce lien', 'ffap-2015' ),
		'items_list'            => __( 'Liste de lien', 'ffap-2015' ),
		'items_list_navigation' => __( 'Navigation des liens', 'ffap-2015' ),
		'filter_items_list'     => __( 'Filtrer les liens', 'ffap-2015' ),
	);
	$args = array(
		'label'                 => __( 'Lien', 'ffap-2015' ),
		'description'           => __( 'Liste de liens composant la blogroll', 'ffap-2015' ),
		'labels'                => $labels,
		'supports'              => array( 'title' ),
		'taxonomies'            => array( 'link-cat' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 15,
		'show_in_admin_bar'     => false,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'links', $args );

}
add_action( 'init', 'links_post_type', 0 );

function create_links_taxonomies() {
    register_taxonomy(
		'link-category',
		'links',
		array(
			'label' => __( 'États' ),
			'rewrite' => array( 'slug' => 'link-cat' ),
			'hierarchical' => true,
            'show_ui'      => true,
		)
	);
}
add_action( 'init', 'create_links_taxonomies', 0 );
// ----------------------------------------------------------------
?>