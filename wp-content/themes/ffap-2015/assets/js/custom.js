/* ==========================================================================
 *  Default JavaScripts
============================================================================= */

/* VARIABLES
---------------------------------------------------- */



/* FONCTIONS
---------------------------------------------------- */

jQuery(document).ready(function ($) {
    // MENU SHRINKING SCROLL  
    $(".header--mainnav").sticky({
        topSpacing: 0,
        center: true,
        className: "scroll",
        responsiveWidth: true,
        getWidthFrom: '.page'
    });

    // MENU MOBILE 
    if ($(window).width() <= 768) {
        $("li.menu-item-has-children").each(function () {
            $(this).find("> a").wrap('<span></span>');
        });
        $("<i class='fa fa-chevron-down'></i>").insertAfter("li.menu-item-has-children > span");

        $('.header--mainnav_logo').insertBefore('.header--smallnav');

        $(".header--mainnav_responsive-header").click(function () {
            $(".header--mainnav_collapsed").slideToggle();
            if ($(".header--mainbar").hasClass('submenu-hidden')) {
                $(".header--mainbar").removeClass('submenu-hidden').addClass('submenu-visible');
            } else {
                $(".header--mainbar").removeClass('submenu-visible').addClass('submenu-hidden');
            }
        });
        $("li.menu-item-has-children i").click(function () {
            $(this).parent().find("ul.sub-menu").slideToggle();
        });
    }

    // GESTION DES IMAGES ARTICLES
    $('.single--content img').each(function () {
        $(this).addClass(this.width > this.height ? 'horizontal' : 'vertical');
    });
    $('.single--content .single--media img.aligncenter').each(function () {
        $(this).parent().addClass('aligncenter');
    });
    $('.single--content .single--media img.alignnone').each(function () {
        $(this).parent().addClass('alignnone');
    });
    $(".contact-form").find("span").replaceWith("<span class='required'>*</span>");

    // FANCYBOX 
    $(".single--media, .tiled-gallery a, .ftg-lightbox").fancybox({
        padding: 0,
        margin: 0
    });

    // BACK TO TOP 
    if ($('.back-to-top').length) {
        var scrollTrigger = 100, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('.back-to-top').addClass('show');
                } else {
                    $('.back-to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('.back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 700);
        });
        $('a[href*=\\#]:not([href=\\#])').each(function() {
            $(this).click(function () {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        $('html,body').animate({
                            scrollTop: target.offset().top - 90
                        }, 400);
                        return false;
                    }
                }
            });
        });
    }
    if ($(window).width() >= 768) {
        var $grid = $('.grid').imagesLoaded( function() {
            $('.grid').isotope({
                layoutMode: 'packery',
                packery: {
                    gutter: '.gutter-sizer'
                },
                itemSelector: '.grid-item',
                percentPosition: true
            });
        });
    }
});


jQuery(document).ready(function ($) {
    $('.single--content img').each(function () {
        $(this).addClass(this.width > this.height ? 'horizontal' : 'vertical');
    });
});