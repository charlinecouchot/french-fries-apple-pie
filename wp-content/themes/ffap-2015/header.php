<?php
/*
 * @package WordPress
 * @subpackage FFAP-2015
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>assets/js/vendor/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
   <?php if (is_single()) : ?>
        <div id="fb-root"></div>
   <?php endif; ?>
    <div class="page">
        <header class="header" role="banner">
            <?php if ( has_nav_menu( 'smallnav' ) ) : ?>
                <nav class="header--smallnav">
                    <?php
                        wp_nav_menu( array(
                            'container'      => '',
                            'theme_location' => 'smallnav'
                        ) );
                    ?>
                </nav>
            <?php endif; ?>
            <?php if ( has_nav_menu( 'mainnav' ) ) : ?>
                <div class="header--mainnav">
                    <div class="header--mainnav_part header--mainnav_logo">
                        <?php if ( is_front_page() && is_home() ) { ?><h1><?php } ?>
                            <a href="<?php echo get_site_url()?>">
                                <?php bloginfo('name'); ?>
                                <span class="full"></span>
                                <span class="small"></span>
                            </a>
                        <?php if ( is_front_page() && is_home() ) { ?></h1><?php } ?>
                    </div>
                    <div class="header--mainnav_responsive">
                        <div class="header--mainnav_responsive-header">
                            <i class="fa fa-bars"></i> <span><?php _e('Menu', 'ffap-2015') ?></span>
                        </div>
                        <div class="header--mainnav_collapsed">
                        <nav>
                            <div class="header--mainnav_part header--mainnav_left">
                                <ul>
                                    <?php
                                        wp_nav_menu( array(
                                            'container'      => '',
                                            'theme_location' => 'mainnav',
                                            'items_wrap'     => '%3$s'
                                        ) );
                                    ?>
                                </ul>
                            </div>
                            <div class="header--mainnav_part header--mainnav_right"><ul></ul></div>
                            <script>
                                var items = jQuery('.header--mainnav .header--mainnav_part > ul > li');
                                var total_menu_items = items.length;
                                var half_position_even=total_menu_items/2;
                                var half_position_odd=(total_menu_items-1)/2; 
                                var ex = /^\d+$/;
                                function resize() {
                                    if("matchMedia" in window) {
                                        if(window.matchMedia("(min-width:1023px)").matches) {
                                            if (ex.test(total_menu_items/2)) {
                                              items.slice(half_position_even).appendTo( jQuery(".header--mainnav_right > ul") );
                                            } else {
                                              items.slice(half_position_odd).appendTo( jQuery(".header--mainnav_right > ul") );
                                            }
                                        } else {
                                            jQuery('.header--mainnav_right > ul > li').appendTo( jQuery(".header--mainnav_left > ul"));
                                        }
                                    }
                                }
                                jQuery(document).ready(resize);
                                jQuery(window).resize(resize);
                            </script>
                        </nav>
                    </div>
                    </div>
                    <div class="header--search">
                        <?php get_search_form(); ?>
                    </div>
                </div>
            <?php endif; ?>
        </header>
 
	    <section class="body">
            <div class="body--container">
                <?php if ( is_front_page() || is_home() || is_post_type_archive() || is_archive() || is_category() || is_page() || is_search() ) : ?>
                    <?php $query = new WP_Query(array(
                        'meta_key'       => 'featured', 
                        'meta_value' => 'oui',
                        'posts_per_page'=> 3
                    )); ?>
                    <?php if ($query->have_posts()) : ?>
                       <section class="featured">
                            <ul>
                                <?php while ($query->have_posts()) : $query->the_post(); ?>
                                <?php if (has_post_thumbnail()) : ?>
                                    <li class="featured--item">
                                        <div class="featured--category"><span><?php the_category(', ') ?></span></div>
                                        <figure class="featured--img">
                                            <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('home-article', array('title' => '', 'class' => 'featured--item_img')); ?></a>
                                        </figure>
                                        <div class="featured--details">
                                            <a href="<?php the_permalink() ?>">
                                                <h3 class="featured--title"><?php the_title(); ?></h3>
                                                <p class="featured--excerpt">
                                                    <?php
                                                    $excerpt = get_the_excerpt();
                                                    $getlength = strlen($excerpt);
                                                    $thelength = 80;
                                                    echo mb_substr($excerpt, 0, $thelength);
                                                    if ($getlength > $thelength) echo "...";
                                                ?>
                                                </p>
                                            </a>
                                        </div>
                                    </li>
                                <?php endif; endwhile; ?>
                                <?php wp_reset_postdata(); ?>
                            </ul>
                            </section>
                        <?php endif; ?>
                <?php endif; ?>
