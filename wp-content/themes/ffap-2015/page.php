<?php get_header(); ?>
<section class="page-content" id="page-content">
   <?php the_title( '<h1 class="page--title"><span>', '</span></h1>' ); ?>
   <div class="content--container"><?php the_content(); ?></div>
</section> 
<aside class="sidebar">
    <?php if ( is_active_sidebar( 'actu-sidebar' ) ) : ?>
        <?php dynamic_sidebar( 'actu-sidebar' ); ?>
    <?php endif; ?>
</aside>
<?php get_footer(); ?>
