<?php get_header(); ?>
<section class="main-content">
   <h2 class="home-section--title"><span><?php _e( 'Tous les articles dans « ' . single_cat_title( '', false ) . ' »', 'ffap' ); ?></span></h2>
    <?php if (have_posts()) : ?>
        <section class="home-section">
            <ul id="article-list">
                <?php
                    while ( have_posts() ) : the_post();
                        get_template_part( 'content', get_post_format() );
                    endwhile;
                ?>
            </ul>
            <button class="btn btn-turquoise" id="infinite-handle">  
                <?php _e( 'Charger plus d\'articles', 'ffap' ); ?>
            </button>  
        </section> 
    <?php else :?>
    <p class="no-articles"><?php _e( 'Il n\'y a aucun article dans cette catégorie pour le moment.', 'ffap' ); ?></p>
    <?php endif; ?>
</section>  
<aside class="sidebar">
    <?php if ( is_active_sidebar( 'actu-sidebar' ) ) : ?>
        <?php dynamic_sidebar( 'actu-sidebar' ); ?>
    <?php endif; ?>
</aside>
<?php get_footer(); ?>
