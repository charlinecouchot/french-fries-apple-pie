<?php get_header(); ?>

<article id="post-<?php the_ID(); ?>" class="single">
    <?php if ( has_post_thumbnail() ) { ?> 
       <figure class="single--image">
            <?php the_post_thumbnail('article-header', array('title' => '', 'class' => 'single--image_img')); ?>
        </figure>
    <?php } ?>
    <header class="single--header">
       <div class="single--meta">
            <div>
                <div class="single--date"><?php _e( 'Publié le' , 'ffap-2015'); ?> <?php the_date(); ?></div>
                <div class="single--categories"><?php _e('Dans '); the_category(', '); ?></div>
                <div class="single--comments">
                    <a href="<?php comments_link(); ?>"><?php comments_number( 'aucun commentaire', '1 commentaire', '% commentaires' ); ?></a>
                </div>
            </div>
        </div>
        <?php the_title( '<h1 class="single--title">', '</h1>' ); ?>
    </header>
    <div class="single--content">
        <?php remove_filter( 'the_content', 'sharing_display', 19 ); ?>
        <?php remove_filter( 'the_excerpt', 'sharing_display', 19 ); ?>
        <?php the_content(); ?>
    </div>
</article>
<section class="single--sharing">
   <p class="single--sharing_title"><?php _e('Partager cet article', 'ffap-2015') ?></p>
    <ul>
        <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnail' ); ?>
        <li class="facebook"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink($post->ID)); ?>" target="_blank"><i class="icon-facebook" aria-hidden="true">Facebook</i></a></li>
        <li class="twitter"><a href="https://twitter.com/intent/tweet?text=<?php the_title(); ?>&url=<?php get_permalink(); ?>&via=Fries_and_Pie" target="_blank"><i class="icon-twitter" aria-hidden="true"></i>Twitter</a></li>
        <li class="hellocoton"><a href="http://www.hellocoton.fr/vote" target="_blank" ><i class="icon-hellocoton" aria-hidden="true"></i>Hellocoton</a></li>
        <li class="pinterest"><a href="http://pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink($post->ID)); ?>&media=<?php echo $image[0]; ?>&description=<?php the_title(); ?>"><i class="icon-pinterest" aria-hidden="true"></i>Pinterest</a></li>
        <li class="email"><?php echo do_shortcode('[send_to_friend]'); ?></li>
    </ul>
</section>
<section class="single--nav_container" aria-hidden="true">
    <?php /*$nav = get_the_post_navigation( array(
            'next_text' => '<div class="single--nav next" aria-hidden="true">' . __( 'Article suivant', 'ffap-2015' ) . '</div>',
            'prev_text' => '<div class="single--nav prev" aria-hidden="true">' . __( 'Article précédent', 'ffap-2015' ) . '</div>',
            'screen_reader_text' => __( 'A' )
        ) );
        $nav = str_replace('<h2 class="screen-reader-text">A</h2>', '', $nav);
        echo $nav;*/ ?>
        <?php $prevPost = get_previous_post(false);
            if($prevPost) {
                $args = array(
                    'posts_per_page' => 1,
                    'include' => $prevPost->ID
                );
                $prevPost = get_posts($args);
                foreach ($prevPost as $post) {
                    setup_postdata($post);
        ?>
            <div class="single--nav_item prev">
                <a href="<?php the_permalink(); ?>">
                    <span class="single--nav_heading"><?php _e( 'Article précédent', 'ffap-2015' ); ?></span>
                    <span class="single--nav_title"><?php the_title(); ?></span>
                </a>
            </div>
        <?php
                    wp_reset_postdata();
                } //end foreach
            } // end if

            $nextPost = get_next_post(false);
            if($nextPost) {
                $args = array(
                    'posts_per_page' => 1,
                    'include' => $nextPost->ID
                );
                $nextPost = get_posts($args);
                foreach ($nextPost as $post) {
                    setup_postdata($post);
        ?>
            <div class="single--nav_item next">
                <a href="<?php the_permalink(); ?>">
                    <span class="single--nav_heading"><?php _e( 'Article suivant', 'ffap-2015' ); ?></span>
                    <span class="single--nav_title"><?php the_title(); ?></span>
                </a>
            </div>
        <?php
                    wp_reset_postdata();
                } //end foreach
            } // end if
        ?>
</section>
<section class="single--author">
    <div class="single--author_img">
        <?php echo get_avatar( get_the_author_meta('email'), 100 ); ?>
    </div>
    <div class="single--author_body">
        <span class="single--author_name"><?php _e('Article publié par ','ffap-2015'); ?> <?php the_author(); ?></span>
        <p class="single--author_desc"><?php the_author_meta('description'); ?></p>
    </div>
</section>
<?php echo do_shortcode( '[jetpack-related-posts]' ); ?>
<section class="single--comments">
    <?php  if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
    ?>
</section>
<?php get_footer(); ?>
