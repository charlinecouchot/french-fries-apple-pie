            </div>
        </section>
        <footer class="footer" id="footer">
           <div class="footer--outercontainer">
               <div class="footer--container">
                   <p class="left">© <?php bloginfo('name'); ?> - <?php echo date('Y'); ?>, <?php _e( 'tous droits réservés' , 'ffap-2015'); ?></p>
                       <?php
                        wp_nav_menu( array(
                            'container'      => '',
                            'theme_location' => 'footer'
                        ) );
                    ?>
               </div>
               <div class="footer--logo"></div>
           </div>
        </footer>
    </div>
    
<?php wp_footer(); ?>
<a class="back-to-top"></a>
</body>
</html> 
 