<?php
/*
Template Name: Page On parle de nous
*/ get_header();?>
<section class="fullwidth-content page--aboutus" id="page-content">
    <?php the_title( '<h1 class="page--title"><span>', '</span></h1>' ); ?>
    <div class="fullwidth-content--container grid">
       <div class="grid-sizer"></div>
       <div class="gutter-sizer"></div>
        <?php 
            $repeater = get_field('references');
            $order = array();

            foreach( $repeater as $i => $row ) {
                $order[ $i ] = $row['reference-date'];
            }

            array_multisort( $order, SORT_DESC, $repeater );
            if( $repeater ):
        ?>
            <?php foreach( $repeater as $i => $row ): ?>
                <?php 
                    $title = $row['reference-title'];
                    $date = $row['reference-date'];
                    $blog = $row['reference-blog'];
                    $url = $row['reference-url'];
                    $image = $row['reference-image'];
                    $size = $row['reference-size'];
                    $dateformatstring = "d F Y";
                    $unixtimestamp = strtotime($row['reference-date']);
                ?>
                    <div class="reference--item grid-item <?php if($size == "small") { echo 'grid-item--small'; } else if($size == "big") { echo 'grid-item--big'; } ?>">
                        <figure class="reference--img">
                            <a href="<?php echo $url; ?>"><?php echo wp_get_attachment_image( $image, 'references', false, array('class' => 'reference--item_img', 'alt' => $title, 'scale' => '0') ); ?></a>
                        </figure>
                        <div class="reference--details">
                            <a href="<?php echo $url; ?>">
                                <h3 class="reference--title"><?php echo $title; ?></h3>
                                <p class="reference--excerpt">
                                    <span class="reference--date"><?php echo date_i18n($dateformatstring, $unixtimestamp); ?></span>
                                    <span class="reference--blog"><?php echo $blog; ?></span>
                                </p>
                            </a>
                        </div>
                    </div>
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
</section>
<?php get_footer(); ?> 