<?php
/*
Template Name: Page de liens
*/ get_header(); ?>
<section class="fullwidth-content page--links" id="page-content">
   <?php the_title( '<h1 class="page--title"><span>', '</span></h1>' ); ?>
    <div class="links-content--container small-margin">
        <div class="page--links_intro"><?php the_content(); ?></div>
    </div>
       
    <div class="page--links_map">
        <div class="mapcontainer">
            <div class="map">
                <span>La carte n'est visible que si vous activez JavaScript.</span>
            </div>
        </div>
    </div>
    
    <div class="links-content--container">    
        <div class="page--links_sections" style="display:none">
           <?php $custom_terms = get_terms('link-category');
            foreach($custom_terms as $custom_term) {
                wp_reset_query();
                $args = array('post_type' => 'links','tax_query' => array(array('taxonomy' => 'link-category','field' => 'slug','terms' => $custom_term->slug)));
                $loop = new WP_Query($args);
                if($loop->have_posts()) { ?>
                    <div class="links--section" id="<?php echo $custom_term->slug; ?>">
                        <div class="links--section_inner">
                            <h2 class="links--section_title"><?php echo $custom_term->name; ?></h2>
                            <ul>
                                <?php while($loop->have_posts()) : $loop->the_post(); ?>
                                    <li><a href="http://<?php the_field('link-url'); ?>"><?php the_field('link-name'); ?></a><?php if( get_field('link-city') ){echo " - " . get_field('link-city');} ?></li>
                                <?php endwhile; ?>
                            </ul>
                        </div>
                    </div>
                <?php } ?> 
            <?php } ?>
        </div>
        <script>
            jQuery(document).ready(function($){
                $(".mapcontainer").mapael({
                    map : {
                        name : "usa_states",
                        defaultArea: {
                            attrs : {
                                fill:'#D6D6D6',
                                stroke : "#fff", 
                                "stroke-width" : 1
                            },
                            attrsHover : {
                                fill:'#D6D6D6',
                            },
                            
                        },
                        defaultPlot: {
                            type : "svg",
                            path: "M 24.267286,27.102843 15.08644,22.838269 6.3686216,27.983579 7.5874348,17.934248 0,11.2331 9.9341158,9.2868473 13.962641,0 l 4.920808,8.8464793 10.077199,0.961561 -6.892889,7.4136777 z",
                            width: 30,
                            height: 30,
                            attrs : {
                                fill:'#CD950C',
                                cursor:'pointer',
                                "stroke-width" : 0
                            },
                            attrsHover : {
                                fill:'#ae7f00',
                                "stroke-width" : 0
                            },
                            eventHandlers: {
                                click: function (e, id, mapElem, textElem, elemOptions) {
                                    if (typeof elemOptions.stateClass != 'undefined') {
                                        var elemId = "#" + $('#' + elemOptions.stateClass).attr("id");
                                        console.log(elemId);
                                        $.fancybox.open([
                                            {href : elemId}
                                        ]);
                                    }
                                }
                            }
                        }
                    },
                    plots: {
                        <?php $custom_terms = get_terms('link-category');
                        foreach($custom_terms as $custom_term) {
                            wp_reset_query();
                            $args = array('post_type' => 'links','tax_query' => array(array('taxonomy' => 'link-category','field' => 'slug','terms' => $custom_term->slug)));
                            $loop = new WP_Query($args);
                            if($loop->have_posts()) { ?>
                                '<?php echo $custom_term->slug; ?>' : {
                                    stateClass:"<?php echo $custom_term->slug; ?>",
                                    tooltip:  "<?php echo $custom_term->name; ?>",
                                    latitude: <?php the_field('latitude', 'link-category_'.$custom_term->term_id) ?>,
                                    longitude: <?php the_field('longitude', 'link-category_'.$custom_term->term_id) ?>},
                            <?php } ?> 
                        <?php } ?> 
                    }
                    
                });
            });
        </script>
    </div>
</section>
<?php get_footer(); ?> 