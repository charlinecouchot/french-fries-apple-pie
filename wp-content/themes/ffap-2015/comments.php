<?php
/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="single--comments">

	<?php if ( have_comments() ) : ?>
		<h1 class="comments--title">
			<?php
				printf( _nx( 'Un commentaire', '%1$s commentaires', get_comments_number(), 'comments title', 'ffap-2015' ),
					number_format_i18n( get_comments_number() ), get_the_title() );
			?>
		</h1>

		<ul class="comments--list">
			<?php
				wp_list_comments( array(
					'style'       => 'ul',
					'short_ping'  => true,
					'avatar_size' => 63,
                    'type'        => 'comment',
                    'callback'    => 'commentsTemplate'
				) ); ?>
		</ul>

		<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
            <div class="comments--pagination">
            <?php paginate_comments_links( array('prev_text' => __('Précédent', 'ffap-2015'), 'next_text' => __('Suivant', 'ffap-2015'))); ?>
            </div>
        <?php endif; ?>

	<?php endif; // have_comments() ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Les commentaires sont clos pour cette publication.', 'ffap-2015' ); ?></p>
	<?php endif; ?>

	<?php
        $commenter = wp_get_current_commenter();
        $req = get_option( 'require_name_email' );
        $aria_req = ( $req ? " aria-required='true'" : '' ); 
    
        $fields =  array(

          'author' =>
            '<div class="comment--form-author form--line"><div class="form--label-line"><label for="author">' . __( 'Nom', 'ffap-2015' ) . '</label> ' .
            ( $req ? '<span class="required">*</span></div>' : '</div>' ) .
            '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
            '" size="30"' . $aria_req . ' /></div>',

          'email' =>
            '<div class="comment--form-email form--line"><div class="form--label-line"><label for="email">' . __( 'Adresse email de contact', 'ffap-2015' ) . '</label> ' .
            ( $req ? '<span class="required">*</span></div>' : '</div>' ) .
            '<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
            '" size="30"' . $aria_req . ' /></div>',

          'url' =>
            '<div class="comment--form-url form--line"><div class="form--label-line"><label for="url">' . __( 'Site Web', 'ffap-2015' ) . '</label></div>' .
            '<input id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
            '" size="30" /></div>', 
        );

        $args = array(
          'id_form'           => 'commentform',
          'id_submit'         => 'submit',
          'class_submit'      => 'submit',
          'name_submit'       => 'submit',
          'title_reply'       => __( 'Laissez-moi un petit mot', 'ffap-2015' ),
          'title_reply_to'    => __( 'Laisser un petit mot à %s', 'ffap-2015' ),
          'cancel_reply_link' => __( 'Cancel Reply' ),
          'label_submit'      => __( 'Post Comment' ),
          'format'            => 'xhtml',

          'comment_field' =>  '<div class="comment--form-comment form--line"><div class="form--label-line"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label> ' .
            ( $req ? '<span class="required">*</span></div>' : '</div>' ) . '<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true">' .
            '</textarea></div>',

          'must_log_in' => '<p class="must-log-in">' . 
            sprintf(
              __( 'You must be <a href="%s">logged in</a> to post a comment.' ),
              wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
            ) . '</p>',

          'logged_in_as' => '<p class="logged-in-as">' .
            sprintf(
            __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ),
              admin_url( 'profile.php' ),
              $user_identity,
              wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) )
            ) . '</p>',

          
          'fields' => apply_filters( 'comment_form_default_fields', $fields ),
        );
        comment_form($args);
    ?>

</div><!-- .comments-area -->
