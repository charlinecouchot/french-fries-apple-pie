<?php
// Nettoyage du <head> --------------------------------------------
    function removeHeadLinks() {
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');

    // Script no-js / js class
    function alx_html_js_class () {
        echo '<script>document.documentElement.className = document.documentElement.className.replace("no-js","js");</script>'. "\n";
    }
    add_action( 'wp_head', 'alx_html_js_class', 1 );

    // LAISSER WORDPRESS GERER LE TAG <title>
	add_theme_support( 'title-tag' );
// ----------------------------------------------------------------

// Traductions ----------------------------------------------------
   load_theme_textdomain( 'ffap', TEMPLATEPATH . '/languages' );

    $locale = get_locale();
    $locale_file = TEMPLATEPATH . "/languages/$locale.php";
    if ( is_readable($locale_file) ) {
        require_once($locale_file);
    }
// ----------------------------------------------------------------

// Retrait des attributs width et height des miniatures d'images --
    function clean_img_width_height($string){
        return preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)\>/i', '<$1$4$7>',$string);
    }
// ----------------------------------------------------------------

// Tailles d'images -----------------------------------------------
    add_theme_support('post-thumbnails');
    add_image_size( 'home-article', 747, 495, true );
    add_image_size( 'article-header', 1000, 400, true );
    add_image_size( 'references', 560, 490, true );

// ----------------------------------------------------------------

// Titre de la page -----------------------------------------------
if ( version_compare( $GLOBALS['wp_version'], '4.1', '<' ) ) :
	function wp_title( $title, $sep ) {
		if ( is_feed() ) {
			return $title;
		}
		global $page, $paged;
		$title .= get_bloginfo( 'name', 'display' );
		$site_description = get_bloginfo( 'description', 'display' );
		if ( $site_description && ( is_home() || is_front_page() ) ) {
			$title .= " $sep $site_description";
		}
		if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
			$title .= " $sep " . sprintf( esc_html__( 'Page %s', 'sdv' ), max( $paged, $page ) );
		}
		return $title;
	}
	add_filter( 'wp_title', 'wp_title', 10, 2 );
	/**
	 * Title shim for sites older than WordPress 4.1.
	 *
	 * @link https://make.wordpress.org/core/2014/10/29/title-tags-in-4-1/
	 * @todo Remove this function when WordPress 4.3 is released.
	 */
	function render_title() {
		?>
		<title><?php wp_title( '|', true, 'right' ); ?></title>
		<?php
	}
	add_action( 'wp_head', 'render_title' );
endif;
// ----------------------------------------------------------------

// Menus personnalisés --------------------------------------------    
    if (function_exists('register_nav_menus')) {
        register_nav_menus(array(
            register_nav_menus( array(
                'mainnav' 	=> __( 'Menu Principal', 'ffap' ),
                'smallnav' => __( 'Petit Menu', 'ffap' ),
                'footer'  	=> __( 'Pied de page', 'ffap' ),
            ) )
        )); 
    }
// ----------------------------------------------------------------

// Custom post type -----------------------------------------------
require_once get_template_directory() . '/assets/admin/post-types.php';
// ----------------------------------------------------------------

// Widgets --------------------------------------------------------
require_once get_template_directory() . '/assets/admin/widgets.php';
// ----------------------------------------------------------------

// Scripts et styles ----------------------------------------------
require_once get_template_directory() . '/assets/admin/scripts-styles.php';
// ----------------------------------------------------------------

// Filtres et fonction dans l'article -----------------------------
require_once get_template_directory() . '/assets/admin/content-filters.php';
// ----------------------------------------------------------------

// Commentaires ---------------------------------------------------
require_once get_template_directory() . '/assets/admin/comments.php';
// ----------------------------------------------------------------

// Boutons WYSIWYG ------------------------------------------------
require_once get_template_directory() . '/assets/admin/wysiwyg.php';
// ----------------------------------------------------------------

// Custom post type -----------------------------------------------
require_once get_template_directory() . '/assets/admin/post-types.php';
// ----------------------------------------------------------------

// Recommandations plugins ----------------------------------------
require_once get_template_directory() . '/assets/admin/reco-plugins.php'; 
// ----------------------------------------------------------------

// Options de site ------------------------------------------------
require_once get_template_directory() . '/assets/admin/theme-options.php';
// ----------------------------------------------------------------

?>