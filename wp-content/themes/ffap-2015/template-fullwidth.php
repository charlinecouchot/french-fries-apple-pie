<?php
/*
Template Name: Page pleine largeur
*/ get_header(); ?>
<section class="fullwidth-content" id="page-content">
   <?php the_title( '<h1 class="page--title"><span>', '</span></h1>' ); ?>
   <div class="fullwidth-content--container"><?php the_content(); ?></div>
</section>
<?php get_footer(); ?> 