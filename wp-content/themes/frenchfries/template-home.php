<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
/* Template Name: Page d'accueil */
get_header(); ?>

	<div id="primary" class="site-content">
<?php if(function_exists('show_flexslider_rotator')) echo show_flexslider_rotator( 'homepage' );?>
		<div id="content" role="main">
		<?php $args = array (
                'posts_per_page' => '5'
        ); ?>
        <?php $query = new WP_Query($args); ?>
        <?php if ($query->have_posts()) : ?>

			<?php /* Start the Loop */ ?>
			<?php while ( $query->have_posts() ) : $query->the_post(); ?>
				

                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header class="entry-header">

                        <h2 class="entry-title" style="margin-bottom:10px;float:left">
                            <img  class="icone-titre"  src="http://www.frenchfriesandapplepie.com/wp-content/uploads/2014/icone_titre.jpg" alt="" style="margin-top:-2px; margin-right:5px;float:left" /><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentytwelve' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
                        </h2>

                        <a href="<?php the_permalink(); ?>"><span class="pibfi_pinterest"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'taille-perso' ); } ?>
                        <span class="xc_pin" onclick="pin_this(event, 'http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&amp;media=<?php $image_id = get_post_thumbnail_id(); $image_url = wp_get_attachment_image_src($image_id,'large', true); echo $image_url[0];  ?>&amp;description=<?php the_title(); ?>')"></span></span></a>
                    </header><!-- .entry-header -->
                    <div class="entry-summary">
                        <?php the_excerpt(); ?>
                    </div><!-- .entry-summary -->
                    <div class="comments-link" div style="float: left">
                        <img src="http://www.frenchfriesandapplepie.com/wp-content/uploads/2014/comment.jpg" alt="" style="float:left;margin-right: 5px;margin-top: -3px;" /><?php comments_popup_link( '<span class="leave-reply">' . __( 'Leave a reply', 'twentytwelve' ) . '</span>', __( '1 Reply', 'twentytwelve' ), __( '% Replies', 'twentytwelve' ) ); ?>
                    </div>
                    <div style="float: right;text-transform: uppercase"><span class="date" ><?php the_time('j F Y') ?></span></div><!-- .comments-link -->

                </article><!-- #post -->



			<?php endwhile; ?>

			<?php twentytwelve_content_nav( 'nav-below' ); ?>

		<?php else : ?>

			<article id="post-0" class="post no-results not-found">

			<?php if ( current_user_can( 'edit_posts' ) ) :
				// Show a different message to a logged-in user who can add posts.
			?>
				<header class="entry-header">
					<h1 class="entry-title"><?php _e( 'No posts to display', 'twentytwelve' ); ?></h1>
				</header>

				<div class="entry-content">
					<p><?php printf( __( 'Ready to publish your first post? <a href="%s">Get started here</a>.', 'twentytwelve' ), admin_url( 'post-new.php' ) ); ?></p>
				</div><!-- .entry-content -->

			<?php else :
				// Show the default message to everyone else.
			?>
				<header class="entry-header">
					<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentytwelve' ); ?></h1>
				</header>

				<div class="entry-content">
					<p><?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'twentytwelve' ); ?></p>
					<?php get_search_form(); ?>
				</div><!-- .entry-content -->
			<?php endif; // end current_user_can() check ?>

			</article><!-- #post-0 -->

		<?php endif; // end have_posts() check ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
