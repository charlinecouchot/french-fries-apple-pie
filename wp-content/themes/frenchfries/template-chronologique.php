<?php
/*
Template Name: Archives chronologique
*/
?>
<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<div id="primary" class="site-content">
		<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>
<?php

$previous_year = $year = 0;
$previous_month = $month = 0;
$ul_open = false;

$myposts = get_posts('numberposts=-1&orderby=post_date&order=DESC');

?>

<?php foreach($myposts as $post) : ?>	

	<?php

	setup_postdata($post);

	$year = mysql2date('Y', $post->post_date);
	$month = mysql2date('n', $post->post_date);
	$day = mysql2date('j', $post->post_date);

	?>

	<?php if($year != $previous_year || $month != $previous_month) : ?>

		<?php if($ul_open == true) : ?>
		</ul>
		<?php endif; ?>

		<h1 style="font-size:24px;font-family: 'Hannah-Regular'; margin: 20px 0 20px 20px"><?php the_time('F Y'); ?></h1>

		<ul>

		<?php $ul_open = true; ?>

	<?php endif; ?>

	<?php $previous_year = $year; $previous_month = $month; ?>

	<li style="font-size:14px;margin: 5px 0 5px 40px"><span><?php the_time('j F'); ?> - </span> <span><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span></li>

<?php endforeach; ?>
	</ul>
			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
