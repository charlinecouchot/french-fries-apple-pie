<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
	</div><!-- #main .wrapper -->
</div><!-- #page -->
</div>


<div id="footer">
<div class="footer">


		<nav class="main-navigation2" role="navigation">
			<ul id="menu-menu" class="nav-menu">
				<li id="menu-item-3675" class="menu-item  menu-item-3675"><a href="http://www.frenchfriesandapplepie.com/archives/"><img src="http://www.frenchfriesandapplepie.com/wp-content/uploads/2014/footer_icone.jpg" alt="" style="margin: 0 5px -8px 0" />ARCHIVES</a></li>
				<li id="menu-item-3684" class="menu-item  menu-item-3684"><a href="http://www.frenchfriesandapplepie.com/contact/"><img src="http://www.frenchfriesandapplepie.com/wp-content/uploads/2014/footer_icone.jpg" alt="" style="margin: 0 5px -8px 0" />CONTACT</a></li>
				<li id="menu-item-3685" class="menu-item  menu-item-3685"><a href="http://www.frenchfriesandapplepie.com/partenaires/"><img src="http://www.frenchfriesandapplepie.com/wp-content/uploads/2014/footer_icone.jpg" alt="" style="margin: 0 5px -8px 0" />PARTENAIRES</a></li>
				<li id="menu-item-3685" class="menu-item  menu-item-3685"><a href="http://www.frenchfriesandapplepie.com/liens/"><img src="http://www.frenchfriesandapplepie.com/wp-content/uploads/2014/footer_icone.jpg" alt="" style="margin: 0 5px -8px 0" />LIENS</a></li>
</ul>
		</nav><!-- #site-navigation -->

<div style="text-align:center; margin-top:13%;text-align:center; margin-top:13%;padding-bottom:10px">&copy; French Fries and Apple Pie - All Rights Reserved - Template : <a href="http://www.malderagraphistes.com/" target="_blank">malderagraphistes</a> - Développement : <a href="http://www.sylvaingarcia.name/" target="_blank">www.sylvaingarcia.name</a></div>
</div>
</div>

<?php wp_footer(); ?>
<script type='text/javascript' src='http://www.sylvaingarcia.name/wordpress/laetitia/wp-content/themes/twentytwelve/js/navigation2.js?ver=1.2'></script>
</body>
</html>
