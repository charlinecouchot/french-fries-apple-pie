<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>


	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
		<div class="featured-post">
			<?php _e( 'Featured post', 'twentytwelve' ); ?>
		</div>
		<?php endif; ?>
		<header class="entry-header">

			<h1 class="entry-title" style="margin-bottom:10px;float:left">
				<img  class="icone-titre"  src="http://www.frenchfriesandapplepie.com/wp-content/uploads/2014/icone_titre.jpg" alt="" style="margin-top:-2px; margin-right:5px;float:left" /><a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentytwelve' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
			</h1>

			<a href="<?php the_permalink(); ?>"><span class="pibfi_pinterest"><?php if ( has_post_thumbnail() ) { the_post_thumbnail( 'taille-perso' ); } ?>
<span class="xc_pin" onclick="pin_this(event, 'http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&amp;media=<?php $image_id = get_post_thumbnail_id(); $image_url = wp_get_attachment_image_src($image_id,'large', true); echo $image_url[0];  ?>&amp;description=<?php the_title(); ?>')">

</span></span></a>
			<?php if ( is_single() ) : ?>

			<?php else : ?>

			<?php endif; // is_single() ?>
			<?php if ( comments_open() ) : ?>

			<?php endif; // comments_open() ?>
		</header><!-- .entry-header -->

		<?php if ( is_search() ) : // Only display Excerpts for Search ?>
		<div class="entry-summary">
			<?php the_excerpt(); ?>
		</div><!-- .entry-summary -->
		<?php else : ?>
		<div class="entry-content">
			<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentytwelve' ) ); ?>
			<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'twentytwelve' ), 'after' => '</div>' ) ); ?>
		</div><!-- .entry-content -->
		<?php endif; ?>

				<div class="comments-link" div style="float: left">
					<img src="http://www.frenchfriesandapplepie.com/wp-content/uploads/2014/comment.jpg" alt="" style="float:left;margin-right: 5px;
margin-top: -3px;" /><?php comments_popup_link( '<span class="leave-reply">' . __( 'Leave a reply', 'twentytwelve' ) . '</span>', __( '1 Reply', 'twentytwelve' ), __( '% Replies', 'twentytwelve' ) ); ?>
				</div><div style="float: right;text-transform: uppercase"><span class="date" ><?php the_time('j F Y') ?></span></div><!-- .comments-link -->

	</article><!-- #post -->


